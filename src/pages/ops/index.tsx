import type { GetServerSideProps } from 'next'
import Link from 'next/link'

import { Operator, operators } from '~/data/operators'

type Props = Readonly<{
	operators: readonly Operator[]
}>

const List = ({ operators }: Props) => (
	<div>
		<h1>operators</h1>
		<ul>
			{operators.map(op => (
				<li key={op.id}>
					<Link href={`/ops/${op.id}`}>
						<a>{op.name}</a>
					</Link>
				</li>
			))}
		</ul>
		<hr />
		<p>
			<Link href="/">
				<a>home</a>
			</Link>
		</p>
	</div>
)

export default List

export const getServerSideProps: GetServerSideProps<Props> = async () => ({
	props: {
		operators,
	},
})
