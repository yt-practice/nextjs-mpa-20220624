import type { GetServerSideProps } from 'next'
import Link from 'next/link'

import { Operator, operators } from '~/data/operators'

type Props = Readonly<{
	op: Operator
}>

const Item = ({ op }: Props) => (
	<div>
		<h1>{op.name}</h1>
		<p>
			{op.sex} {op.className}
		</p>
		<hr />
		<p>
			<Link href="/ops">
				<a>operators</a>
			</Link>
		</p>
		<p>
			<Link href="/">
				<a>home</a>
			</Link>
		</p>
	</div>
)

export default Item

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
	const id = Number(ctx.query.id)
	const op = operators.find(op => op.id === id)
	if (op) return { props: { op } }
	return { notFound: true }
}
