import Link from 'next/link'

const Home = () => (
	<div>
		<h1>home</h1>
		<hr />
		<p>
			<Link href="/about">
				<a>about</a>
			</Link>
		</p>
		<p>
			<Link href="/ops">
				<a>operators</a>
			</Link>
		</p>
	</div>
)

export default Home
