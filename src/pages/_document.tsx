import { HtmlContext } from 'next/dist/shared/lib/html-context'
import { Head, Html, Main, NextScript } from 'next/document'
import { useContext } from 'react'

const Document = () => {
	const c = useContext(HtmlContext)
	return (
		<HtmlContext.Provider value={{ ...c, unstable_runtimeJS: false }}>
			<Html>
				<Head />
				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		</HtmlContext.Provider>
	)
}

export default Document
