export const operators = [
	{ id: 1, name: 'ヤトウ', className: '先鋒', sex: '女' },
	{ id: 2, name: 'ドゥリン', className: '術師', sex: '女' },
	{ id: 3, name: 'ノイルホーン', className: '重装', sex: '男' },
	{ id: 4, name: 'レンジャー', className: '狙撃', sex: '男' },
	{ id: 5, name: '12F', className: '術師', sex: '男' },
] as const

export type Operator = typeof operators[number]
